<?php 

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Dashboard extends CI_Controller {
    
        private function check_login() {
            if (!$this->session->userdata('logged_in')) {
                redirect('login/login');
            }
        }

        public function index()
        {
            $data["title"] = "Dashboard";
            $this->load->view('templates/header',$data);
            $this->load->view('Dashboard/index');
            $this->load->view('templates/footer');
        }
    
    }
    
    /* End of file Dashboard.php */
    
?>