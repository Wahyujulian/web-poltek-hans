<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Jurusan_model"); //load model jurusan
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data jurusan";
        //ambil fungsi getAll untuk menampilkan semua data jurusan
        $data["data_jurusan"] = $this->Jurusan_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/jurusan
        $this->load->view('jurusan/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $jurusan = $this->Jurusan_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($jurusan->rules()); //menerapkan rules validasi pada jurusan_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada jurusan_model
        if ($validation->run()) {
            $jurusan->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data jurusan berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("jurusan");
        }
        $data["title"] = "Tambah Data jurusan";
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('jurusan/add', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id = null)
    // {
    //     if (!isset($id)) redirect('jurusan');

    //     $jurusan = $this->jurusan_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($jurusan->rules());

    //     if ($validation->run()) {
    //         $jurusan->update();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data jurusan berhasil disimpan.
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("jurusan");
    //     }
    //     $data["title"] = "Edit Data jurusan";
    //     $data["data_jurusan"] = $jurusan->getById($id);
    //     if (!$data["data_jurusan"]) show_404();
    //     $this->load->view('templates/header', $data);
    //     // $this->load->view('templates/menu');
    //     $this->load->view('jurusan/edit', $data);
    //     $this->load->view('templates/footer');
    // }

    public function edit($id)
    {
        $data['title'] = 'Edit Data jurusan';
        $data['jurusan'] = $this->Jurusan_model->getById($id);
        $jurusan = $this->Jurusan_model;
        $validation = $this->form_validation;
        $validation->set_rules($jurusan->rules());

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('jurusan/edit', $data);
            $this->load->view('templates/footer',);
        } else {
            $this->Jurusan_model->update($id);
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('jurusan');
        }
    }

    // public function delete()
    // {
    //     $id = $this->input->get('id_jurusan');
    //     if (!isset($id)) show_404();
    //     $this->jurusan_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data jurusan berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Jurusan_model->deleteDataJurusan($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('jurusan');
    }
}

/* End of file Controllername.php */
