<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->model('Login_model');
    }

    public function index()
    {
        $this->load->view('login/index.php');
    }

    // public function login()
    // {
    //     if($this->input->post()){
    //         $username = $this->input->post('username');
    //         $password = $this->input->post('password');
    //     }

    //     $user = $this->Login_model->check_login($username, $password);
    //     if ($user) {
    //         $userdata = array(
    //             'id_user' => $user->id,
    //             'username' => $user->username,
    //         );

    //         $this->session->set_userdata($userdata);
    //         redirect('dashboard/index');
    //     } else {
    //         $data['error_message'] = 'Salah Username atau Password';
    //         $this->load->view('login/index.php', $data);
    //     }
    // }

    public function login()
    {
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
        }

        $user = $this->Login_model->check_login($username, $password);
        if ($user) {
            // Check the role_id of the user and load the corresponding view

            $userdata = array(
                'id_user' => $user->id,
                'username' => $user->username,
                'role_id' => $user->role_id
            );

            $this->session->set_userdata($userdata);

            if ($user->role_id == 1) {
                redirect('dashboard/index'); // Assuming you have an 'admin/dashboard' controller/method for the admin view
            } elseif ($user->role_id == 2) {
                redirect('mahasiswa/dashboard'); // Assuming you have a 'mahasiswa/dashboard' controller/method for the mahasiswa view
            } elseif ($user->role_id == 3) {
                redirect('pimpinan/dashboard'); // Assuming you have a 'pimpinan/dashboard' controller/method for the pimpinan view
            } elseif ($user->role_id == 4) {
                redirect('dosen/dashboard'); // Assuming you have a 'dosen/dashboard' controller/method for the dosen view
            } else {
                // Handle any other role_id that doesn't match the above cases
                $data['error_message'] = 'Role ID tidak dikenali';
                $this->load->view('login/index.php', $data);
            }
        } else {
            $data['error_message'] = 'Salah Username atau Password';
            $this->load->view('login/index.php', $data);
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}

/* End of file Controllername.php */
