<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prodi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Prodi_model"); //load model prodi
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data prodi";
        //ambil fungsi getAll untuk menampilkan semua data prodi
        $data["data_prodi"] = $this->Prodi_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/prodi
        $this->load->view('prodi/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $prodi = $this->Prodi_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($prodi->rules()); //menerapkan rules validasi pada prodi_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada prodi_model
        if ($validation->run()) {
            $prodi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data prodi berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("prodi");
        }
        $data["title"] = "Tambah Data prodi";
        $data['datajurusan'] = $this->Prodi_model->tampildata();
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('prodi/add', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id = null)
    // {
    //     if (!isset($id)) redirect('prodi');

    //     $prodi = $this->prodi_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($prodi->rules());

    //     if ($validation->run()) {
    //         $prodi->update();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data prodi berhasil disimpan.
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("prodi");
    //     }
    //     $data["title"] = "Edit Data prodi";
    //     $data["data_prodi"] = $prodi->getById($id);
    //     if (!$data["data_prodi"]) show_404();
    //     $this->load->view('templates/header', $data);
    //     // $this->load->view('templates/menu');
    //     $this->load->view('prodi/edit', $data);
    //     $this->load->view('templates/footer');
    // }

    public function edit($id)
    {
        $data['title'] = 'Edit Data prodi';
        $data['prodi'] = $this->Prodi_model->getById($id);
        $prodi = $this->Prodi_model;
        $validation = $this->form_validation;
        $validation->set_rules($prodi->rules());

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('prodi/edit', $data);
            $this->load->view('templates/footer',);
        } else {
            $this->prodi_model->update($id);
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('prodi');
        }
    }

    // public function delete()
    // {
    //     $id = $this->input->get('id_prodi');
    //     if (!isset($id)) show_404();
    //     $this->prodi_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data prodi berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Prodi_model->deleteDataprodi($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('prodi');
    }
}

/* End of file Controllername.php */
