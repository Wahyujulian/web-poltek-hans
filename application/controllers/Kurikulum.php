<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kurikulum extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Kurikulum_model"); //load model kurikulum
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data Kurikulum";
        //ambil fungsi getAll untuk menampilkan semua data kurikulum
        $data["data_kurikulum"] = $this->Kurikulum_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/kurikulum
        $this->load->view('kurikulum/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $Kurikulum = $this->Kurikulum_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($Kurikulum->rules()); //menerapkan rules validasi pada kurikulum_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada kurikulum_model
        if ($validation->run()) {
            $Kurikulum->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data kurikulum berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("kurikulum");
        }
        $data["title"] = "Tambah Data Kurikulum";
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('kurikulum/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('kurikulum');

        $Kurikulum = $this->Kurikulum_model;
        $validation = $this->form_validation;
        $validation->set_rules($Kurikulum->rules());

        if ($validation->run()) {
            $Kurikulum->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data kurikulum berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("kurikulum");
        }
        $data["title"] = "Edit Data Kurikulum";
        $data["data_kurikulum"] = $Kurikulum->getById($id);
        if (!$data["data_kurikulum"]) show_404();
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        $this->load->view('kurikulum/edit', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id)
    // {
    //     $data['title'] = 'Edit Data kurikulum';
    //     $data['kurikulum'] = $this->Kurikulum_model->getById($id);
    //     $Kurikulum = $this->Kurikulum_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($Kurikulum->rules());

    //     if ($this->form_validation->run() == FALSE) {
    //         $this->load->view('templates/header', $data);
    //         $this->load->view('kurikulum/edit', $data);
    //         $this->load->view('templates/footer',);
    //     } else {
    //         $this->Kurikulum_model->update($id);
    //         $this->session->set_flashdata('flash', 'Diubah');
    //         redirect('kurikulum');
    //     }
    // }

    // public function delete()
    // {
    //     $id = $this->input->get('id_kurikulum');
    //     if (!isset($id)) show_404();
    //     $this->Kurikulum_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data kurikulum berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Kurikulum_model->deleteDataKurikulum($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('kurikulum');
    }
}

/* End of file Controllername.php */
