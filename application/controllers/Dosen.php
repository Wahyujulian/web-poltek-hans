<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Dosen_model"); //load model dosen
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data dosen";
        //ambil fungsi getAll untuk menampilkan semua data dosen
        $data["data_dosen"] = $this->Dosen_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/dosen
        $this->load->view('dosen/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $dosen = $this->Dosen_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($dosen->rules()); //menerapkan rules validasi pada dosen_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada dosen_model
        if ($validation->run()) {
            $dosen->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data dosen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen");
        }
        $data["title"] = "Tambah Data dosen";
        $data['dataprodi'] = $this->Dosen_model->tampildata();
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('dosen/add', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id = null)
    // {
    //     if (!isset($id)) redirect('dosen');

    //     $dosen = $this->dosen_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($dosen->rules());

    //     if ($validation->run()) {
    //         $dosen->update();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data dosen berhasil disimpan.
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("dosen");
    //     }
    //     $data["title"] = "Edit Data dosen";
    //     $data["data_dosen"] = $dosen->getById($id);
    //     if (!$data["data_dosen"]) show_404();
    //     $this->load->view('templates/header', $data);
    //     // $this->load->view('templates/menu');
    //     $this->load->view('dosen/edit', $data);
    //     $this->load->view('templates/footer');
    // }

    public function edit($id)
    {
        $data['title'] = 'Edit Data dosen';
        $data['dosen'] = $this->Dosen_model->getById($id);
        $dosen = $this->Dosen_model;
        $validation = $this->form_validation;
        $validation->set_rules($dosen->rules());

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('dosen/edit', $data);
            $this->load->view('templates/footer',);
        } else {
            $this->Dosen_model->update($id);
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('dosen');
        }
    }

    // public function delete()
    // {
    //     $id = $this->input->get('id_dosen');
    //     if (!isset($id)) show_404();
    //     $this->dosen_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data dosen berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Dosen_model->deleteDatadosen($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('dosen');
    }
}

/* End of file Controllername.php */
