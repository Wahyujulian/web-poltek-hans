<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matakuliah extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Matakuliah_model"); //load model matakuliah
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data matakuliah";
        //ambil fungsi getAll untuk menampilkan semua data matakuliah
        $data["data_matakuliah"] = $this->Matakuliah_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/matakuliah
        $this->load->view('matakuliah/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $matakuliah = $this->Matakuliah_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($matakuliah->rules()); //menerapkan rules validasi pada matakuliah_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada matakuliah_model
        if ($validation->run()) {
            $matakuliah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data matakuliah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("matakuliah");
        }
        $data["title"] = "Tambah Data matakuliah";
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('matakuliah/add', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id = null)
    // {
    //     if (!isset($id)) redirect('matakuliah');

    //     $matakuliah = $this->matakuliah_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($matakuliah->rules());

    //     if ($validation->run()) {
    //         $matakuliah->update();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data matakuliah berhasil disimpan.
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("matakuliah");
    //     }
    //     $data["title"] = "Edit Data matakuliah";
    //     $data["data_matakuliah"] = $matakuliah->getById($id);
    //     if (!$data["data_matakuliah"]) show_404();
    //     $this->load->view('templates/header', $data);
    //     // $this->load->view('templates/menu');
    //     $this->load->view('matakuliah/edit', $data);
    //     $this->load->view('templates/footer');
    // }

    public function edit($id)
    {
        $data['title'] = 'Edit Data matakuliah';
        $data['matakuliah'] = $this->Matakuliah_model->getById($id);
        $matakuliah = $this->Matakuliah_model;
        $validation = $this->form_validation;
        $validation->set_rules($matakuliah->rules());

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('matakuliah/edit', $data);
            $this->load->view('templates/footer',);
        } else {
            $this->Matakuliah_model->update($id);
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('matakuliah');
        }
    }

    // public function delete()
    // {
    //     $id = $this->input->get('id_matakuliah');
    //     if (!isset($id)) show_404();
    //     $this->matakuliah_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data matakuliah berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Matakuliah_model->deleteDatamatakuliah($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('matakuliah');
    }
}

/* End of file Controllername.php */
