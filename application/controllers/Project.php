<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Project_model"); //load model project
    }

    //method pertama yang akan di eksekusi
    public function index()
    {

        $data["title"] = "List Data project";
        //ambil fungsi getAll untuk menampilkan semua data project
        $data["data_project"] = $this->Project_model->getAll();
        //load view header.php pada folder views/templates
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        //load view index.php pada folder views/project
        $this->load->view('project/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $Project = $this->Project_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($Project->rules()); //menerapkan rules validasi pada project_model
        //kondisi jika semua kolom telah divalidasi, maka akan menjalankan method save pada project_model
        if ($validation->run()) {
            $Project->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data project berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("project");
        }
        $data["title"] = "Tambah Data project";
        $this->load->view('templates/header', $data);
        //$this->load->view('templates/menu');
        $this->load->view('project/add', $data);
        $this->load->view('templates/footer');
    }

    // public function edit($id = null)
    // {
    //     if (!isset($id)) redirect('project');

    //     $project = $this->project_model;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($project->rules());

    //     if ($validation->run()) {
    //         $project->update();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data project berhasil disimpan.
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("project");
    //     }
    //     $data["title"] = "Edit Data project";
    //     $data["data_project"] = $project->getById($id);
    //     if (!$data["data_project"]) show_404();
    //     $this->load->view('templates/header', $data);
    //     // $this->load->view('templates/menu');
    //     $this->load->view('project/edit', $data);
    //     $this->load->view('templates/footer');
    // }

    public function edit($id)
    {
        $data['title'] = 'Edit Data project';
        $data['project'] = $this->Project_model->getById($id);
        $project = $this->Project_model;
        $validation = $this->form_validation;
        $validation->set_rules($project->rules());

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('project/edit', $data);
            $this->load->view('templates/footer',);
        } else {
            $this->Project_model->update($id);
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('project');
        }
    }

    // public function delete()
    // {
    //     $id = $this->input->get('id_project');
    //     if (!isset($id)) show_404();
    //     $this->project_model->delete($id);
    //     $msg['success'] = true;
    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //     Data project berhasil dihapus.
    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //     </button></div>');
    //     $this->output->set_output(json_encode($msg));
    // }

    public function delete($id)
    {
        $this->Project_model->deleteDataproject($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('project');
    }
}

/* End of file Controllername.php */
