<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>Kurikulum</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('kurikulum'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post">
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Nama prodi</legend>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_prodi" name="nama_prodi" value=" <?= set_value('nama_prodi'); ?>">
                                    <small class="text-danger">
                                        <small class="text-danger">
                                            <?php echo form_error('nama_prodi') ?>
                                        </small>
                                </div>
                            </div>
                        </fieldset>

                        <div class="form-group row">
                            <label for="tgl_berdiri" class="col-sm-2 col-form-label">Tanggal Berdiri</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="tgl_berdiri" name="tgl_berdiri" value=" <?= set_value('tgl_berdiri'); ?>">
                                <small class="text-danger">
                                    <?php echo form_error('tgl_berdiri') ?>
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_berdiri" class="col-sm-2 col-form-label">Deskripsi Prodi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="desc_prodi" name="desc_prodi" value=" <?= set_value('tgl_berdiri'); ?>">
                                <small class="text-danger">
                                    <?php echo form_error('desc_prodi') ?>
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_berdiri" class="col-sm-2 col-form-label">Jurusan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="table_jurusan_id_jurusan" name="table_jurusan_id_jurusan" value=" <?= set_value('tgl_berdiri'); ?>">
                                <small class="text-danger">
                                    <?php echo form_error('table_jurusan_id_jurusan') ?>
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10 offset-md-2">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>