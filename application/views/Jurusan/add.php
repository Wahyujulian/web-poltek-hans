<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>jurusan</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('jurusan'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    //create form
                    $attributes = array('id_jurusan' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off");
                    echo form_open('', $attributes);
                    ?>

                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Nama jurusan</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_jurusan" name="nama_jurusan" value=" <?= set_value('nama_jurusan'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('nama_jurusan') ?>
                                    </small>
                            </div>
                        </div>
                    </fieldset>>

                    <div class="form-group row">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>