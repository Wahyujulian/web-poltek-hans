<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>Jurusan</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('jurusan'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Username</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="username" name="username" value=" <?= set_value('username'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('username') ?>
                                    </small>
                            </div>
                        </div>
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Password</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="password" name="password" value=" <?= set_value('pasword'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('password') ?>
                                    </small>
                            </div>
                        </div>
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Role</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="role" name="role" value=" <?= set_value('role'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('role') ?>
                                    </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10 offset-md-2">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>