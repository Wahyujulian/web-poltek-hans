<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>Kurikulum</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('kurikulum'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post">
                    <div class="form-group row">
                        <label for="kode_kurikulum" class="col-sm-2 col-form-label">Kode Kurikulum</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_kurikulum" name="kode_kurikulum" value=" <?= set_value('kode_kurikulum'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('kode_kurikulum'); ?>
                            </small>
                        </div>
                    </div>

                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Nama Kurikulum</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_kurikulum" name="nama_kurikulum" value=" <?= set_value('nama_kurikulum'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('nama_kurikulum'); ?>
                                    </small>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group row">
                        <label for="tgl_berlaku" class="col-sm-2 col-form-label">Tanggal Berlaku</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tgl_berlaku" name="tgl_berlaku" value=" <?= set_value('tgl_berlaku'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('tgl_berlaku'); ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>