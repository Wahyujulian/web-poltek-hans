<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>matakuliah</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('matakuliah'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    //create form
                    $attributes = array('id_matkul' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off");
                    echo form_open('', $attributes);
                    ?>
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Kode Matakuliah</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="kode_matkul" name="kode_matkul" value=" <?= set_value('kode_matkul'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('kode_matkul') ?>
                                    </small>
                                </small>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Nama matakuliah</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_matkul" name="nama_matkul" value=" <?= set_value('nama_matkul'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('nama_matkul') ?>
                                    </small>
                                </small>
                            </div>
                        </div>
                    </fieldset>

                     <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Semester</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="semester" name="semester" value=" <?= set_value('semester'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('semester') ?>
                                    </small>
                                </small>
                            </div>
                        </div>
                    </fieldset> 
                    
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">SKS</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="sks" name="sks" value=" <?= set_value('sks'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('sks') ?>
                                    </small>
                                </small>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">FIle RPS</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="file_rps" name="file_rps" value=" <?= set_value('file_rps'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('file_rps') ?>
                                    </small>
                                </small>
                            </div>
                        </div>
                    </fieldset>

                    <br>

                    <div class="form-group row">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>