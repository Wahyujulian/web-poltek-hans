<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>Mahasiswa</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('mahasiswa'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    //create form
                    $attributes = array('id_mahasiswa' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off");
                    echo form_open('', $attributes);
                    ?>
                    <div class="form-group row">
                        <label for="Nama" class="col-sm-2 col-form-label">NIM</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="NIM" name="NIM" value=" <?= set_value('NIM'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('NIM') ?>
                            </small>
                        </div>
                    </div>

                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Nama</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" value=" <?= set_value('nama_mahasiswa'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('nama_mahasiswa') ?>
                                    </small>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Tempat Lahir</legend>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value=" <?= set_value('tempat_lahir'); ?>">
                                <small class="text-danger">
                                    <small class="text-danger">
                                        <?php echo form_error('tempat_lahir') ?>
                                    </small>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group row">
                        <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value=" <?= set_value('tgl_lahir'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('tgl_lahir') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                <option value="Pria" selected disabled>Pilih</option>
                                <option value="Pria" <?php if (set_value('jenis_kelamin') == "Pria") : echo "selected";
                                                        endif; ?>>Pria</option>
                                <option value="Wanita" <?php if (set_value('jenis_kelamin') == "Wanita") : echo "selected";
                                                        endif; ?>>Wanita</option>
                            </select>
                            <small class="text-danger">
                                <?php echo form_error('jenis_kelamin') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="Alamat" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="alamat" name="alamat" rows="3"><?= set_value('alamat'); ?></textarea>
                            <small class="text-danger">
                                <?php echo form_error('alamat') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="table_user_id_user" class="col-sm-2 col-form-label">Role</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="table_user_id_user" name="table_user_id_user" value=" <?= set_value('table_user_id_user'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('table_user_id_user') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="Prodi" class="col-sm-2 col-form-label">Prodi Studi</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="prodi" aria-label="Default select example">
                                <option selected>Pilih Program Studi</option>
                                <?php foreach ($dataprodi as $options) : ?>
                                    <option value="<?php echo $options['id_prodi']; ?>"><?php echo $options['nama_prodi']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <small class="text-danger">
                                <?php echo form_error('table_prodi_id_prodi') ?>
                            </small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10 offset-md-2">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>