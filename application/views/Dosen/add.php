<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>jurusan</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('jurusan'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    //create form
                    $attributes = array('id_jurusan' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off");
                    echo form_open('', $attributes);
                    ?>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">NIP Dosen</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="NIP_dosen" name="NIP_dosen" value=" <?= set_value('NIP_dosen'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('NIP_dosen') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Nama Dosen</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_dosen" name="nama_dosen" value=" <?= set_value('nama_dosen'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('nama_dosen') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Tanggal Lahir</legend>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value=" <?= set_value('tgl_lahir'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('tgl_lahir') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Tempat Lahir</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value=" <?= set_value('tempat_lahir'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('tempat_lahir') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value=" <?= set_value('jenis_kelamin'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('jenis_kelamin') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Alamat</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="alamat" name="alamat" value=" <?= set_value('alamat'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('alamat') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Agama</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="agama" name="agama" value=" <?= set_value('agama'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('agama') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Status</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="status" name="status" value=" <?= set_value('status'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('status') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Nomor HP:</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nomor_hp" name="nomor_hp" value=" <?= set_value('nomor_hp'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('nomor_hp') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Jabatan:</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="jabatan" name="jabatan" value=" <?= set_value('jabatan'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('jabatan') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">role:</legend>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="table_user_id_user" name="table_user_id_user" value=" <?= set_value('table_user_id_user'); ?>">
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('table_user_id_user') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <legend class="col-form-label col-sm-2 pt-0">Program Studi:</legend>
                        <div class="col-sm-10">
                            <select class="form-select" name="prodi" aria-label="Default select example">
                                <option selected>Pilih Program Studi</option>
                                <?php foreach ($dataprodi as $options) : ?>
                                    <option value="<?php echo $options['id_prodi']; ?>"><?php echo $options['nama_prodi']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <small class="text-danger">
                                <small class="text-danger">
                                    <?php echo form_error('table_prodi_id_prodi') ?>
                                </small>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>