<div class="container pt-5">
    <h3><?= $title ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a>project</a></li>
            <li class="breadcrumb-item "><a href="<?= base_url('project'); ?>">List Data</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Data</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    //create form
                    $attributes = array('id_project' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off");
                    echo form_open('', $attributes);
                    ?>
                    <div class="form-group row">
                        <label for="judul_project" class="col-sm-2 col-form-label">Judul Project</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="judul_project" name="judul_project" value=" <?= set_value('kode-project'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('judul_project') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tgl_mulai" class="col-sm-2 col-form-label">Tanggal Berlaku</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value=" <?= set_value('tgl_berlaku'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('tgl_mulai') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="client" class="col-sm-2 col-form-label">client</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="client" name="client" value=" <?= set_value('client'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('client') ?>
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file_rpp" class="col-sm-2 col-form-label">file_rpp</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="file_rpp" name="file_rpp" value=" <?= set_value('file_rpp'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('file_rpp') ?>
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file_proposal" class="col-sm-2 col-form-label">file_proposal</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="file_proposal" name="file_proposal" value=" <?= set_value('file_proposal'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('file_proposal') ?>
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">status</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="status" name="status" value=" <?= set_value('status'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('status') ?>
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahun_ajaran" class="col-sm-2 col-form-label">tahun_ajaran</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tahun_ajaran" name="tahun_ajaran" value=" <?= set_value('tahun_ajaran'); ?>">
                            <small class="text-danger">
                                <?php echo form_error('tahun_ajaran') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="hasil_project" class="col-sm-2 col-form-label">hasil project</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="hasil_project" name="hasil_project"disabled value="-">
                            <small class="text-danger">
                                <?php echo form_error('hasil_project') ?>
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url_hasil" class="col-sm-2 col-form-label">url hasil</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="url_hasil" name="url_hasil" disabled value="-">
                            <small class="text-danger">
                                <?php echo form_error('url_hasil') ?>
                            </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                        </div>
                    </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>