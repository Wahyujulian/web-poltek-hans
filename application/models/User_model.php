<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    private $table = 'table_user';

    public function rules()
    {
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'role',
                'label' => 'Role',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_user" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_user");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "username" => $this->input->post('username'),
            "password" => $this->input->post('password'),
            "role" => $this->input->post('role')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "username" => $this->input->post('username'),
            "password" => $this->input->post('password'),
            "role" => $this->input->post('role')
        );
        $this->db->where('id_user', $this->input->post('id_user'));
        $this->db->update('table_user', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_user" => $id));
    // }

    public function deleteDatauser($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('table_user');
    }
}

/* End of file ModelName.php */
