<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan_model extends CI_Model
{

    private $table = 'table_jurusan';

    public function rules()
    {
        return [
            [
                'field' => 'nama_jurusan',
                'label' => 'Nama jurusan',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_jurusan" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_jurusan");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "nama_jurusan" => $this->input->post('nama_jurusan')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "nama_jurusan" => $this->input->post('nama_jurusan')
        );
        $this->db->where('id_jurusan', $this->input->post('id_jurusan'));
        $this->db->update('table_jurusan', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_jurusan" => $id));
    // }

    public function deleteDataJurusan($id)
    {
        $this->db->where('id_jurusan', $id);
        $this->db->delete('table_jurusan');
    }
}

/* End of file ModelName.php */
