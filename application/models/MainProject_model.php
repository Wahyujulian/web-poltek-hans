<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MainProject_model extends CI_Model
{

    private $table = 'table_project';

    public function rules()
    {
        return [
            [
                'field' => 'judul_project',  //samakan dengan atribute name pada tags input
                'label' => 'judul project',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ['id_project' => $id])->row_array();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_project");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "judul_project" => $this->input->post('judul_project'),
            "tgl_mulai" => $this->input->post('tgl_mulai'),
            "client" => $this->input->post('client'),
            "file_rpp" => $this->input->post('file_rpp'),
            "file_proposal" => $this->input->post('file_proposal'),
            "status" => $this->input->post('status'),
            "tahun_ajaran" => $this->input->post('tahun_ajaran'),
            "hasil_project" => $this->input->post('hasil_project'),
            "url_hasil" => $this->input->post('url_hasil')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "judul_project" => $this->input->post('judul_project'),
            "tgl_mulai" => $this->input->post('tgl_mulai'),
            "client" => $this->input->post('client'),
            "file_rpp" => $this->input->post('file_rpp'),
            "file_proposal" => $this->input->post('file_proposal'),
            "status" => $this->input->post('status'),
            "tahun_ajaran" => $this->input->post('tahun_ajaran'),
            "hasil_project" => $this->input->post('hasil_project'),
            "url_hasil" => $this->input->post('url_hasil')
        );
        $this->db->where('id_project', $this->input->post('id_project'));
        $this->db->update('table_project', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_project" => $id));
    // }

    public function deleteDataproject($id)
    {
        $this->db->where('id_project', $id);
        $this->db->delete('table_project');
    }
}

/* End of file ModelName.php */
