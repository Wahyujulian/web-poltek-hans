<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model
{

    private $table = 'table_mahasiswa';
    private $table_prodi = 'table_prodi';

    public function rules()
    {
        return [
            [
                'field' => 'NIM',  //samakan dengan atribute name pada tags input
                'label' => 'NIM',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_mahasiswa" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_mahasiswa", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    public function tampildata() {
        $query = $this->db->query("Select * from table_prodi");
        return $query->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "NIM" => $this->input->post('NIM'),
            "nama_mahasiswa" => $this->input->post('nama_mahasiswa'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
            "table_user_id_user" => $this->input->post('table_user_id_user'),
            "table_prodi_id_prodi" => $this->input->post('prodi')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "NIM" => $this->input->post('NIM'),
            "nama_mahasiswa" => $this->input->post('nama_mahasiswa'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
            "table_user_id_user" => $this->input->post('table_user_id_user'),
            "table_prodi_id_prodi" => $this->input->post('table_prodi_id_prodi')
        );
        return $this->db->update($this->table, $data, array('id_mahasiswa' => $this->input->post('id_mahasiswa')));
    }

    //hapus data mahasiswa
    // public function deleteDataMahasiswa($id)
    // {
    //     return $this->db->delete($this->table, array("id_mahasiswa" => $id));
    // }

    public function deleteDataMahasiswa($id)
    {
        $this->db->where('id_mahasiswa', $id);
        $this->db->delete('table_mahasiswa');
    }
}



/* End of file ModelName.php */
