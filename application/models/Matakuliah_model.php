<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Matakuliah_model extends CI_Model
{

    private $table = 'table_matakuliah';

    public function rules()
    {
        return [
            [
                'field' => 'kode_matkul',
                'label' => 'Nama matakuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_matkul',
                'label' => 'Nama matakuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'semester',
                'label' => 'Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks',
                'label' => 'SKS',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_matkul" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_matkul");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "kode_matkul" => $this->input->post('kode_matkul'),
            "nama_matkul" => $this->input->post('nama_matkul'),
            "semester" => $this->input->post('semester'),
            "sks" => $this->input->post('sks'),
            "file_rps" => $this->input->post('file_rps')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "kode_matkul" => $this->input->post('kode_matkul'),
            "nama_matkul" => $this->input->post('nama_matkul'),
            "semester" => $this->input->post('semester'),
            "sks" => $this->input->post('sks'),
            "file_rps" => $this->input->post('file_rps')
        );
        $this->db->where('id_matakuliah', $this->input->post('id_matkul'));
        $this->db->update('table_matakuliah', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_matakuliah" => $id));
    // }

    public function deleteDatamatakuliah($id)
    {
        $this->db->where('id_matkul', $id);
        $this->db->delete('table_matakuliah');
    }
}

/* End of file ModelName.php */
