<?php 
    defined('BASEPATH') or exit('No direct script access allowed');
    class Login_model extends CI_Model {

        public function check_login($username, $password) {
            // Perform the database query to check the credentials
            $query = $this->db->get_where('table_user', array('username' => $username, 'password' => $password));
    
            if ($query->num_rows() == 1) {
                return $query->row(); // Return the user data if the login is successful
            } else {
                return false; // Return false if login fails
            }
        }
    }
?>