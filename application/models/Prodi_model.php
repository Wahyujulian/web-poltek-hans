<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Prodi_model extends CI_Model
{

    private $table = 'table_prodi';

    public function rules()
    {
        return [
            [
                'field' => 'nama_prodi',
                'label' => 'Nama prodi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_berdiri',
                'label' => 'Tanggal Berdiri',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'desc_prodi',
                'label' => 'Deskripsi Prodi',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_prodi" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function tampildata() {
        $query = $this->db->query("Select * from table_jurusan");
        return $query->result_array();
    }

    public function namajurusan() {
        
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_prodi");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "nama_prodi" => $this->input->post('nama_prodi'),
            "tgl_berdiri" => $this->input->post('tgl_berdiri'),
            "desc_prodi" => $this->input->post('desc_prodi'),
            "table_jurusan_id_jurusan" => $this->input->post('jurusan') 
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "nama_prodi" => $this->input->post('nama_prodi'),
            "tgl_berdiri" => $this->input->post('tgl_berdiri'),
            "desc_prodi" => $this->input->post('desc_prodi'),
            "table_jurusan_id_jurusan" => $this->input->post('table_jurusan_id_jurusan')
        );
        $this->db->where('id_prodi', $this->input->post('id_prodi'));
        $this->db->update('table_prodi', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_prodi" => $id));
    // }

    public function deleteDataProdi($id)
    {
        $this->db->where('id_prodi', $id);
        $this->db->delete('table_prodi');
    }
}

/* End of file ModelName.php */
