<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dosen_model extends CI_Model
{

    private $table = 'table_dosen';

    public function rules()
    {
        return [
            [
                'field' => 'nama_dosen',
                'label' => 'Nama dosen',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_dosen" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function tampildata() {
        $query = $this->db->query("Select * from table_prodi");
        return $query->result_array();
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_dosen");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "NIP_dosen" => $this->input->post('NIP_dosen'),
            "nama_dosen" => $this->input->post('nama_dosen'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
            "agama" => $this->input->post('agama'),
            "status" => $this->input->post('status'),
            "nomor_hp" => $this->input->post('nomor_hp'),
            "jabatan" => $this->input->post('jabatan'),
            "table_user_id_user" => $this->input->post('table_user_id_user'),
            "table_prodi_id_prodi" => $this->input->post('prodi')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "NIP_dosen" => $this->input->post('NIP_dosen'),
            "nama_dosen" => $this->input->post('nama_dosen'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "alamat" => $this->input->post('alamat'),
            "agama" => $this->input->post('agama'),
            "status" => $this->input->post('status'),
            "nomor_hp" => $this->input->post('nomor_hp'),
            "jabatan" => $this->input->post('jabatan'),
            "table_user_id_user" => $this->input->post('table_user_id_user'),
            "table_prodi_id_prodi" => $this->input->post('table_prodi_id_prodi')
        );
        $this->db->where('id_dosen', $this->input->post('id_dosen'));
        $this->db->update('table_dosen', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_dosen" => $id));
    // }

    public function deleteDataDosen($id)
    {
        $this->db->where('id_dosen', $id);
        $this->db->delete('table_dosen');
    }
}

/* End of file ModelName.php */
