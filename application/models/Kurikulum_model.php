<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kurikulum_model extends CI_Model
{

    private $table = 'table_kurikulum';

    public function rules()
    {
        return [
            [
                'field' => 'kode_kurikulum',  //samakan dengan atribute name pada tags input
                'label' => 'Kode Kurikulum',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama_kurikulum',
                'label' => 'Nama Kurikulum',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_berlaku',
                'label' => 'Tanggal Berlaku',
                'rules' => 'trim|required'
            ],

        ];
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ['id_kurikulum' => $id])->row_array();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id_kurikulum");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "kode_kurikulum" => $this->input->post('kode_kurikulum'),
            "nama_kurikulum" => $this->input->post('nama_kurikulum'),
            "tgl_berlaku" => $this->input->post('tgl_berlaku'),
            "table_matakuliah_id_matkul" => $this->input->post('table_matakuliah_id_matkul')
        ); 
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "kode_kurikulum" => $this->input->post('kode_kurikulum'),
            "nama_kurikulum" => $this->input->post('nama_kurikulum'),
            "tgl_berlaku" => $this->input->post('tgl_berlaku'),
            "table_matakuliah_id_matkul" => $this->input->post('table_matakuliah_id_matkul')
        );
        $this->db->where('id_kurikulum', $this->input->post('id_kurikulum'));
        $this->db->update('table_kurikulum', $data);
    }

    // hapus data mahasiswa
    // public function delete($id)
    // {
    //     return $this->db->delete($this->table, array("id_kurikulum" => $id));
    // }

    public function deleteDataKurikulum($id)
    {
        $this->db->where('id_kurikulum', $id);
        $this->db->delete('table_kurikulum');
    }
}

/* End of file ModelName.php */
